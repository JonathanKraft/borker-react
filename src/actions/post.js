import types from "../constants/actionTypes";
import { HTTP } from "../utils/httpInstance";
import { toggleLoader } from "./loader";

export const getPost = id => {
  return async dispatch => {
    dispatch(toggleLoader(true));
    const response = await HTTP.get(`posts/${id}`)
    const post = await response.data;
    dispatch({
      type: types.GET_POST,
      post
    });
    dispatch(toggleLoader(false));
  };
};

export const getPosts = () => {
  return async dispatch => {
    const response = await HTTP.get(`posts`)
    dispatch(toggleLoader(true));

    const posts = await response.data;
    dispatch({
      type: types.GET_POSTS,
      posts
    });
    dispatch(toggleLoader(false));
  };

};

export const addPost = post => {
  return async dispatch => {
    const response = await HTTP.post(`posts`, post)
    const data = await response.data;
    dispatch({
      type: types.ADD_POST,
      post: data
    });
  };
};

export const putPost = post => {
  return async dispatch => {
    const response = await HTTP.put(`posts/${post.id}`, post)
    const data = await response.data;
    dispatch({
      type: types.PUT_POST,
      post: data
    });
  };
};

export const deletePost = post => {
  return async dispatch => {
    await HTTP.delete(`posts/${post.id}`)
    dispatch({
      type: types.DELETE_POST,
    });
  };
};

export const selectPost = (post) => {
  return {
    type: types.SELECT_POST,
    post
  };
}

export const addReply = (reply, target) => {
  return async dispatch => {
    await HTTP.post(`posts/reply/${target}`, reply)
    dispatch({
      type: types.REPLY_TO_POST,
    });
  }
};

export const addRebork = (rebork, source) => {
  return async dispatch => {
    await HTTP.post(`posts/rebork/${source}`, rebork)
    dispatch({
      type: types.REBORK_POST,
    });
  }
};