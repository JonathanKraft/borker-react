import types from "../constants/actionTypes";
import { HTTP } from "../utils/httpInstance";

export const login = credentials => {
  return async dispatch => {
    const response = await HTTP.post(`login_check`, credentials)
    const data = await response.data;
    HTTP.defaults.headers.common['Authorization'] = `Bearer = ${data.token}`;
    dispatch({
      type: types.LOGIN_REQUEST,
      token: data.token,
      isLogged: true
    });
  };
};

export const logout = () => {
  return dispatch => {
    dispatch({
      type: types.LOGOUT
    });
  };
};

export const register = user => {
  return async dispatch => {
    
    const response = await HTTP.post(`users`, user)
    const data = await response.data;
    dispatch({
      type: types.REGISTER_REQUEST,
      user: data
    });
  };
};

export const getLoggedUser = () => {
  return async dispatch => {
    const response = await HTTP.get(`users/get-logged`)
    const data = await response.data;
    console.log(data);
    
    dispatch({
      type: types.GET_LOGGED_USER,
      loggedUser: data,
    });
  };
};