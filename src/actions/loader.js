import types from "../constants/actionTypes";

export const toggleLoader = (value) => {
  return {
    type: types.TOGGLE_LOADER,
    value
  };
};