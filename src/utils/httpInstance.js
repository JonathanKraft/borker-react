import axios from 'axios';
import { API_ROOT } from '../constants/api';
import {store} from '../store';

export const HTTP = axios.create({
  baseURL: API_ROOT,
  headers: {
    'Content-type': 'application/json'
    // 'Authorization': `Bearer: ${store.getState().user.token.token}`
  }
});

HTTP.interceptors.request.use(
  config => {
    const token = store.getState().user.token;
      if (token) {
          config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
  },
  error => {
      Promise.reject(error)
  });
