import ReborkButton from '../../components/widgets/ReborkButton'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addRebork } from "../../actions/post";
import { withRouter } from 'react-router-dom'

const mapStateToProps = state => ({
  selectedPost: state.post.selectedPost
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ addRebork }, dispatch)
};

export default withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(ReborkButton));
