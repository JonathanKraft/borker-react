import ReplyButton from '../../components/widgets/ReplyButton'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { selectPost, addReply } from "../../actions/post";
import { withRouter } from 'react-router-dom'

const mapStateToProps = state => ({
  selectedPost: state.post.selectedPost
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ selectPost, addReply }, dispatch)
};

export default withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(ReplyButton));
