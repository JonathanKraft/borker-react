import PostList from '../components/PostList'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPost, getPosts } from "../actions/post";
import { withRouter } from 'react-router-dom'

const mapStateToProps = state => ({
  posts: state.post.posts,
  loader: state.loader.loader,
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getPost, getPosts }, dispatch)
};

export default withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(PostList));
