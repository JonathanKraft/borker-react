import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom'
import { getLoggedUser } from "../actions/user";
import App from "../components/App";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return {
    token: state.user.token,
    isLogged: state.user.isLogged,
    loggedUser: state.user.loggedUser
  };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getLoggedUser }, dispatch);
};

/** Finaly we connect the component with the store */
export default  withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
