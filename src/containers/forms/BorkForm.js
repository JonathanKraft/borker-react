import BorkForm from '../../components/forms/BorkForm'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from 'react-router-dom'
import { addPost } from "../../actions/post";


const mapStateToProps = state => ({
  token: state.user.token
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ addPost }, dispatch)
};

export default withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(BorkForm));
