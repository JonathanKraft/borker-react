import RegisterForm from '../../components/forms/RegisterForm'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { register } from "../../actions/user";

const mapStateToProps = state => ({
  user: state.user.user
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ register }, dispatch)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterForm);
