import LoginForm from '../../components/forms/LoginForm'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { login } from "../../actions/user";
import { withRouter } from 'react-router-dom'

const mapStateToProps = state => ({
  token: state.user.token
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ login }, dispatch)
};

export default withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm));
