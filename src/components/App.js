import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from '../components/Header';
import SideNav from '../components/SideNav';
import Login from '../screens/Login';
import RegisterForm from '../containers/forms/RegisterForm';
import PrivateRoute from './PrivateRoute';
import { Columns } from 'react-bulma-components';
import Home from '../screens/Home';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount = async () => {
    console.log();
    
    if (this.props.isLogged){
       await this.props.getLoggedUser();
    };
  }

  render() {
    return (
      <Columns>
        {this.props.isLogged &&
          <Columns.Column narrow>
            <SideNav />
          </Columns.Column>
        }
        <div className="verticality">
          {this.props.isLogged &&
            <Columns.Column narrow className="header-column">
              <Header />
            </Columns.Column>
          }
            <Router>
              <div className="App">
                <Switch>
                  <Route path="/login" component={Login} />
                  <Route path="/register" component={RegisterForm} />
                  {/* <PrivateRoute exact path="/" authenticated={this.props.isLogged} component={PostList}>
                  </PrivateRoute> */}
                  <PrivateRoute exact path="/" authenticated={this.props.isLogged} component={Home}>
                  </PrivateRoute>
                </Switch>
              </div>
            </Router>
        </div>
      </Columns>

    );
  }
}

export default App;
