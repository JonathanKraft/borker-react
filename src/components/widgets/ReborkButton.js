import React from 'react';
import { Button } from 'react-bulma-components';

class ReplyButton extends React.Component {

  constructor(props) {
    super(props);
  }

  reborkThis = async () => {
    await this.props.addRebork({ content: this.props.content}, this.props.postId);
  }

  render() {
    return (
      <Button onClick={this.reborkThis}>Rebork</Button>
    )
  }
}
export default ReplyButton;