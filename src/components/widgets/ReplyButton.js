import React from 'react';
import { Button } from 'react-bulma-components';

class ReplyButton extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Button onClick={console.log(this.props.replyTo)}>Reply</Button>
    )
  }
}
export default ReplyButton;