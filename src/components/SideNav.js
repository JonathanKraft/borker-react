import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import borker_icon from '../assets/borker.svg'
import { Navbar } from 'react-bulma-components';
import { Link } from 'react-router-dom';


const SideNav = () => {

  return (

    <Navbar>
      <Navbar.Brand>
        <div className="navbar-item">
          <img src={borker_icon} alt="Borker logo" width="50" />
        </div>
        <Navbar.Burger>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </Navbar.Burger>
      </Navbar.Brand>
      <Navbar.Menu>
        <Navbar.Container>
          <div className="navbar-item">
            <Link to="/">
              Home
              </Link>
          </div>
          <div className="navbar-item">
            <Link to="/profile">
              Profile
              </Link>
          </div>
          <div className="navbar-item">
            <Link to="/">
              Manages
              </Link>
          </div>
        </Navbar.Container>
        <Navbar.Container position="end">
          <div className="navbar-item">
            Disconnect
         </div>
        </Navbar.Container>
      </Navbar.Menu>
    </Navbar>
    // <Menu className="aside-menu" role="navigation" aria-label="main navigation">
    //   <Menu.List className="aside-logo" >
    //     <Menu.List.Item>
    //     <img src={borker_icon} alt="Borker logo" width="50" />
    //     </Menu.List.Item>
    //   </Menu.List>
    //   <Menu.List>
    //     <Menu.List.Item>
    //       Home
    //     </Menu.List.Item>
    //     <Menu.List.Item>
    //       Profile
    //     </Menu.List.Item>
    //     <Menu.List.Item>
    //       Manage Lists
    //     </Menu.List.Item>
    //   </Menu.List>

    //   <Menu.List className="aside-menu-end" >
    //     <Menu.List.Item>
    //       Disconnect
    //     </Menu.List.Item>
    //   </Menu.List>
    // </Menu>
  );
}
export default SideNav;