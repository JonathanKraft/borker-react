import React from 'react';
import { Card } from 'react-bulma-components';
import ReplyButton from '../containers/widgets/ReplyButton';
import ReborkButton from '../containers/widgets/ReborkButton';


const PostListItems = (object) =>
  <li key={object.object.id} value={object.object.id} className="bork-card">
    <Card>
      <p className="subtitle is-6 bork-sender">{object.object.user.username} borks: </p>
      <Card.Content>
        {object.object.content}
      </Card.Content>
      <Card.Footer>
        {/* <ReplyButton target={object.object.id}>Reply</ReplyButton> */}
        <ReborkButton content={object.object.content} postId={object.object.id}>rebork</ReborkButton>
      </Card.Footer>
    </Card>
  </li>
export default PostListItems;