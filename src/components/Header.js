import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';

class Header extends React.Component {

  render() {
    return (
      <header className="header">
        <h3 className="title is-4 header-title">title!</h3>
      </header>
    )
  }
}

export default Header;