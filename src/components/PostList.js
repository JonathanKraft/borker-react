import React from 'react';
import Post from './Post';
import Loader from './widgets/Loader';

class PostList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      posts: []
    }
  }

  async componentDidMount() {
    let fetchedPosts = [];
    await this.props.getPosts();

    fetchedPosts = this.props.posts.reverse();
    this.setState({
      posts: fetchedPosts,
    });
  }

  displayPosts = () => {
    return this.state.posts.map((post, index) =>
      <Post object={post} key={index} />
    );
  }

  render() {
    return (
      <div>
        {this.props.loader && 
        <Loader />
        }
        {this.props.posts &&
          <ul>
            {this.displayPosts()}
          </ul>}
      </div>
    )
  }
}

export default PostList;