import React from 'react';
import { Form, Button } from 'react-bulma-components';

class LoginForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      credentials: {
        username: '',
        password: ''
      }
    }
  }
  handleChange = (event) => {
    let { name, value } = event.target;
    const { credentials } = this.state;

    this.setState({
      credentials: {
        ...credentials,
        [name]: value
      }
    });
  }

  sendToRegister = (event) => {
    console.log('register');

    this.props.history.push('/register');
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const { credentials } = this.state;
    await this.props.login(credentials);
    this.props.history.replace('/');
  }

  render() {
    return (
      <form>
        <Form.Field>
          <Form.Label>
            Email:
        </Form.Label>
          <Form.Input placeholder="Email..." name="username" value={this.state.credentials.username} onChange={this.handleChange} />

          <Form.Label>
            Password:
        </Form.Label>
          <Form.Input placeholder="Password..." name="password" type="password" value={this.state.credentials.password} onChange={this.handleChange} />
          <Button onClick={this.handleSubmit} type="submit">Submit</Button>
        </Form.Field>

        <p>You don't have an account? <a onClick={this.sendToRegister}>Click HERE!</a></p>
      </form>
    )
  }
}
export default LoginForm;