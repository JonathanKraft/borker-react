import React from 'react';
import { Form, Button } from 'react-bulma-components';

class RegisterForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {
        username: '',
        email: '',
        password: ''
      },
      confirmPassword: ''
    }

  }

  handleChange = (event) => {
    let { name, value } = event.target;
    const user = this.state.user;
    if (["email","username","password"].includes(name)){
      this.setState({
        user: {
          ...user,
          [name]: value
        },
      });
    }
    else if("confirmPassword" === name){
      this.setState({
        confirmPassword: value
      });
    }
  }

  handleSubmit = () => {
    if(this.state.user.password === this.state.confirmPassword){
      this.props.register(this.state.user);
    }
    else {
      alert('Password and conformation are different')
    }
  }


  render() {
    return (
      <form>
        <Form.Field>
          <Form.Label>
            Email:
        </Form.Label>
          <Form.Input placeholder="Email..." name="email" value={this.state.user.email} onChange={this.handleChange} />
          <Form.Label>
            Username:
        </Form.Label>
          <Form.Input placeholder="Username..." name="username" value={this.state.user.username} onChange={this.handleChange} />

          <Form.Label>
            Password:
        </Form.Label>
          <Form.Input placeholder="Password..." name="password" type="password" value={this.state.user.password} onChange={this.handleChange} />
          <Form.Label>
            Confirm Password:
        </Form.Label>
          <Form.Input placeholder="Confirm Password..." name="confirmPassword" type="password" value={this.state.confirmPassword} onChange={this.handleChange} />

          <Button onClick={this.handleSubmit} type="submit">Submit</Button>
        </Form.Field>
      </form>
    )
  }
}

export default RegisterForm;