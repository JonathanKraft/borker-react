import React from 'react';
import { Form, Button } from 'react-bulma-components';
import { store } from '../../store';

class BorkForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      bork: {
        content: ''
      }
    }
  }
  componentWillMount() {
    console.log(store);
    
  }
  handleChange = (event) => {
    let { name, value } = event.target;
    const { bork } = this.state;

    this.setState({
      bork: {
        ...bork,
        [name]: value
      }
    });
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    console.log(this.state);
    await this.props.addPost(this.state.bork);
  }

  render() {
    return (
      <form>
        <Form.Field>
          <Form.Label>
            {/* Username: {store.loggedUser.username} */}
        </Form.Label>
          <Form.Textarea placeholder="Watcha borking about ..." name="content" value={this.state.bork.content} onChange={this.handleChange} />
          <Button onClick={this.handleSubmit} type="submit">Bork !</Button>
        </Form.Field>
      </form>
    )
  }
}
export default BorkForm;