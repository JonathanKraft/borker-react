const INITIAL_STATE = {
  loader: false
};

export const loader = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'TOGGLE_LOADER':
      return {
        loader: action.value
      };
    default:
      return state;
  }
}