import types from "../constants/actionTypes";

const INITIAL_STATE = {
  post: {},
  posts: {},
  selectedPost: 0
};

export const post = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_POST:
      return {
        ...state,
        post: action.post
      };
    case types.GET_POST:
      return {
        ...state,
        post: action.post
      };
    case types.GET_POSTS:
      return {
        ...state,
        posts: action.posts
      };
    case types.PUT_POST:
      return {
        ...state,
        post: action.post
      };
    case types.DELETE_POST:
      return {
        ...state,
        post: action.post
      };
    case types.REPLY_TO_POST:
      return {
        ...state,
        post: action.post
      };
    case types.REBORK_POST:
      return {
        ...state,
        post: action.post
      };
    case types.SELECT_POST:
      return {
        ...state,
        selectedPost: action.post
      };
    default:
      return state;
  }
};