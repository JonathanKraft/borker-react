import types from "../constants/actionTypes";

const INITIAL_STATE = {
  token: {},
  isLogged: false,
  loggedUser: {}
};

export const user = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        token: action.token,
        isLogged: true
      };
    case types.LOGOUT_REQUEST:
      return {
        INITIAL_STATE
      };
    case types.REGISTER_REQUEST:
      return {
        ...state
      };
    case types.GET_LOGGED_USER:
      return {
        ...state,
        loggedUser: action.loggedUser
      }
    default:
      return state;
  }
};