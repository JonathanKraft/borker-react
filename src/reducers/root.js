import { combineReducers } from "redux";
import { loader } from './loader';
import { post } from './post';
import { user } from './user';


export default combineReducers({
  loader,
  post,
  user

 });