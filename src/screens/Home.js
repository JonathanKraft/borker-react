import React from 'react';
import PostList from '../containers/PostList';
import BorkForm from '../containers/forms/BorkForm';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: []
    }
  }

  render() {
    return (
      <div className="home-page">
        <BorkForm />
        <PostList />
      </div>
    )
  }
}

export default Login;