import React from 'react';
import { Columns } from 'react-bulma-components';
import LoginForm from '../containers/forms/LoginForm';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: []
    }
  }

  render() {
    return (
      <div className="login-page">
        <Columns.Column>
          <h1>BORKER YEAH !</h1>
        </Columns.Column>
        <Columns.Column>
          <LoginForm />
        </Columns.Column>
      </div>
    )
  }
}

export default Login;